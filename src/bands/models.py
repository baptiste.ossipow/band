from django.db import models

class Band(models.Model):
    name = models.CharField(max_length=100)
    slug = models.SlugField()
    concert = models.TextField()
    instrument = models.TextField()
    musician = models.TextField()
    pub_date = models.DateTimeField(auto_now_add=False)

    def __unicode__(self):
        return self.name
