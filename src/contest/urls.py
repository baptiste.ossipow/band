from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
                       url(r'^$', 'django.contrib.auth.views.login',
                           {'template_name': 'index.html'}),
                       
                       url(r'^bands/', include('bands.urls')),

                       url(r'^admin/', include(admin.site.urls)),
)
