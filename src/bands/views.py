from django.shortcuts import render_to_response, get_object_or_404
# -*- coding: utf-8 -*-
from django.template import RequestContext

from .models import Band

def render_auth(request, pTemplate, pParams=None):
    try:
        if pParams:
            request.session['CurrentProject'] = pParams['Project'].name
    except KeyError:
        pass

    return render_to_response(pTemplate, 
                          pParams, 
                          context_instance=RequestContext(request))

def bands_az(request):
    """
    Donne une liste alphabetique des groupes
    """
    return render_auth(request, 'bands/az.htm')



def bands_letter(request, pLetter):
    """
    Affiche les groupes commençant avec la lettre choisie
    """
    lBands = Band.objects.filter(name__istartswith=pLetter)
    return render_auth(request, 'bands/letter.htm', {"Bands": lBands,
                                                     "Letter": pLetter,
                                                     })

def band(request, pBandSlug):
    """
    Affiche les détails sur le groupe choisi.
    """
    try:
        lBand = Band.objects.filter(slug=pBandSlug).select_related()[0]
    except IndexError:
        raise Http404()
    return render_auth(request, 'bands/results.htm', {'Band': lBand,
                                                      })
