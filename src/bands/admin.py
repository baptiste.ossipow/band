from django.contrib import admin

from .models import Band

class BandAdmin(admin.ModelAdmin):
    list_display = ('name', 'concert', 'instrument', 'musician', 'pub_date')
    prepopulated_fields = {'slug': ('name',), }

    class Meta:
        model = Band

admin.site.register(Band, BandAdmin)
