from django.conf.urls import patterns, url

from .models import Band

urlpatterns = patterns('',
                       url(r'^$', 'bands.views.bands_az'),
                       url(r'^([A-Z0-9])/$', 'bands.views.bands_letter'),
#                       url(r'^details/$', 'django.contrib.auth.views.login',
#                           {'template_name': 'bands/detail.html'}),
                       url(r'^([\w-]*)/$', 'bands.views.band'),


)
